import sdl2.sdlmixer as mixer

# Dynamically load libraries needed by the mixer.
if mixer.Mix_Init(0) == -1:
    print("ERR: Mix_Init")
    exit()

# Initialize the mixer API. Use 2 channels (stereo).
if mixer.Mix_OpenAudio(44100, mixer.MIX_DEFAULT_FORMAT, 2, 64) == -1:
    print("ERR: Mix_OpenAudio")
    exit()

# This call won't fail.
mixer.Mix_AllocateChannels(1)

# Set volume of the channel to max. We only allocated 1 channel, so use 0.
mixer.Mix_Volume(0, mixer.MIX_MAX_VOLUME)

beep = mixer.Mix_LoadWAV(b"beep.ogg")
if not beep:
    print("ERR: Failed to load sound effects")
    exit()

while True:
    input()
    mixer.Mix_PlayChannel(0, beep, 0)
