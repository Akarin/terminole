# terminole - Whac-A-Mole in the terminal

## Install

This game uses pySDL2 for audio, and curses for graphics.

On *nix, make sure SDL2 and ncurses are installed.

On Windows, the platform-specific dependency `windows-curses` bundles PDcurses, and `pysdl2-dll` bundles SDL2.

Also make sure that you have `wheels` and `setuptools`. To install:

```
pip install .
```

## Running

TODO add entrypoint

currently run with `python -m terminole`.
