import curses
import json
import sys
from pathlib import Path

import art
from platformdirs import PlatformDirs

import terminole


class GameOverException(Exception):
    def __init__(self, score: float):
        super().__init__()

        self.score = score


def show(stdscr: curses.window, score: float):
    terminole.cleanup(stdscr)

    print(f"Your Score: {score:.0f}")
    name = input("Add your name to the scoreboard: ")

    dirs = PlatformDirs("terminole", "akarin")
    data_dir = Path(dirs.user_data_dir)
    data_dir.mkdir(parents=True, exist_ok=True)
    scores_file = Path(data_dir, "scores.json")

    if not scores_file.is_file():
        scores_file.touch()
        scores_file.write_text("{}", encoding="utf-8")

    with open(scores_file, "r", encoding="utf-8") as fp:
        players = json.load(fp)
        players[name] = score
        # Sort the dictionary according to value
        players = dict(sorted(players.items(), key=lambda item: item[1], reverse=True))

    with open(scores_file, "w", encoding="utf-8") as fp:
        json.dump(players, fp)

    print("\033[H\033[J")  # clear screen
    art.tprint("Scoreboard", font="bigfig")
    print(f"NAME{'SCORE':>26}")

    for _, (name, sc) in enumerate(players.items()):
        print(f"{name:25}", end="")
        print(f"{sc:>5.0f}")

    input()
    sys.exit()
