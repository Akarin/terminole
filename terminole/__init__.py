import curses
import sys

from terminole import holes, scoreboard, status, title

# size of a hole
HOLE_NROWS = 11
HOLE_NCOLS = 35
# number and layout of holes, starts from 0
# TODO: levels
ROWS = 1
COLS = 4

# Initialize curses screen
stdscr = curses.initscr()
curses.noecho()
curses.cbreak()
curses.curs_set(0)


def cleanup(stdscr: curses.window):
    curses.nocbreak()
    stdscr.keypad(False)
    curses.echo()
    curses.curs_set(1)
    curses.endwin()


def arcade(stdscr: curses.window):
    stdscr.clear()
    stdscr.refresh()

    status_bar = status.StatusBar(0, curses.newwin(1, 80, 11, 0))
    holes_arr = []

    for i in range(ROWS):
        for j in range(COLS):
            holes_arr.append(
                holes.Hole(
                    0,
                    curses.newwin(
                        HOLE_NROWS, HOLE_NCOLS, HOLE_NROWS * i, HOLE_NCOLS * j
                    ),
                )
            )

    for hole in holes_arr:
        hole.init()

    while True:
        for hole in holes_arr:
            try:
                hole.tick(status_bar)
            except scoreboard.GameOverException as err:
                scoreboard.show(stdscr, err.score)

        ch = stdscr.getch()

        if ch != -1:
            match ch:
                case 100 | 68:  # dD
                    hole = 0
                case 102 | 70:  # fF
                    hole = 1
                case 106 | 74:  # jJ
                    hole = 2
                case 107 | 75:  # kK
                    hole = 3
                case 99 | 67:  # cC
                    hole = 4
                case 118 | 86:  # vV
                    hole = 5
                case 110 | 78:  # nN
                    hole = 6
                case 109 | 77:  # mM
                    hole = 7
                case 113 | 81:  # qQ
                    cleanup(stdscr)
                    sys.exit()

            try:
                holes_arr[hole].hit(status_bar)
            except (
                TypeError,  # `hole` is not defined, undefined key
                IndexError,  # `hole` maps to a missing hole
            ):
                status_bar.invalid_key(ch)
            except scoreboard.GameOverException as err:
                scoreboard.show(stdscr, err.score)

        curses.napms(32)


def main(stdscr: curses.window):
    # stdscr.keypad(True)

    stdscr.clear()
    # must refresh once here,
    # otherwise everything gets overwritten on the first getch()
    stdscr.refresh()

    mode = title.title(stdscr)

    if mode == 0:
        arcade(stdscr)


curses.wrapper(main)
