import curses

from terminole import scoreboard


class StatusBar:
    score = 0.0
    missed = 0

    def __init__(self, mode: int, win: curses.window):
        self.win = win
        self.mode = mode
        self.update(self.score)

    def update(self, score_change: float):
        self.score += score_change

        self.win.clear()
        self.win.addstr(0, 0, f"SCORE: {self.score:-.0f}", curses.A_REVERSE)

        if self.mode == 0:
            self.win.addstr(0, 15, "* " * (3 - self.missed))

        self.win.refresh()

    def miss(self):
        self.missed += 1

        if self.missed < 3:
            self.update(0)
        else:
            raise scoreboard.GameOverException(self.score)

    def invalid_key(self, key: int):
        self.win.addstr(0, 15, f"Invalid keybind for this mode: {chr(key)}")
        self.win.refresh()
