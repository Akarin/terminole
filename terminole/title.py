import curses
import sys

import art

import terminole


class Menu:
    t_start = art.text2art("Start", font="bigfig") + " " * 15
    t_exit = art.text2art("Exit", font="bigfig") + " " * 12
    t_modes = [
        art.text2art("Arcade", font="bigfig"),
        art.text2art("Free", font="bigfig"),
        art.text2art("Sprint", font="bigfig"),
    ]
    highlight = 0
    """
    0: arcade
    - three lives, moles get faster
    - score is the internal "score"

    1: freeplay
    - moles get faster, more holes overtime
    - score is the time before internal "score" reaches zero

    2: sprint
    - moles are VERY fast
    - one miss / empty hit kills
    - score is the internal "score"
    """
    mode = 0

    def __init__(self, win: curses.window):
        self.win = win
        self.win.clear()
        self.update(0)
        self.mode_update(0)

    def update(self, change: int):
        new_highlight = self.highlight + change

        if new_highlight >= 0 and new_highlight < 2:
            self.highlight = new_highlight
            styles = [0, 0]

            styles[new_highlight] = curses.A_REVERSE

            self.win.addstr(0, 0, self.t_start, styles[0])
            self.win.addstr(10, 0, self.t_exit, styles[1])
            self.win.refresh()

    def mode_update(self, change: int):
        new = self.mode + change

        if new >= 0 and new < 3:
            self.mode = new

            self.win.clear()
            self.update(self.highlight)
            self.win.addstr(5, 0, self.t_modes[new])
            self.win.refresh()


def title(stdscr: curses.window) -> dict:
    ascii_art = art.text2art("Term>\ninole", font="rnd-medium")
    art_win = curses.newwin(24, 55, 0, 0)
    menu = Menu(curses.newwin(22, 20, 0, 55))

    art_win.clear()
    art_win.addstr(ascii_art)
    art_win.refresh()

    stdscr.nodelay(False)

    while True:
        ch = stdscr.getch()

        match ch:
            case curses.KEY_UP:
                menu.update(-1)
            case curses.KEY_DOWN:
                menu.update(1)
            case curses.KEY_LEFT:
                menu.mode_update(-1)
            case curses.KEY_RIGHT:
                menu.mode_update(1)
            case 10:  # Enter
                match menu.highlight:
                    case 0:
                        stdscr.nodelay(True)
                        return menu.mode
                    case 1:
                        terminole.cleanup(stdscr)
                        sys.exit()
