import curses
import time
from contextlib import ExitStack
from pathlib import Path
from random import SystemRandom

import sdl2.sdlmixer as mixer

from terminole import status

ASSET_PATH = Path(Path(__file__).parent, "assets")
ASSET_NAMES = ["hole_empty.txt", "hole_popup.txt", "hammer.txt"]
SFX_NAMES = ["hammer_desk1.ogg", "hammer_desk2.ogg", "hammer_metal.ogg"]
assets = {}
sfx = {}
random = SystemRandom()  # hopefully a fast hardware RNG

# Load assets
with ExitStack() as stack:
    files = [
        stack.enter_context(open(Path(ASSET_PATH, fname), encoding="utf-8"))
        for fname in ASSET_NAMES
    ]

    for fp in files:
        text = fp.read()
        assets[Path(fp.name).name] = text

# Initialize audio
if mixer.Mix_Init(0) == -1:
    raise RuntimeError("SDL2 Error: failed to initialize mixer")

if mixer.Mix_OpenAudio(44100, mixer.MIX_DEFAULT_FORMAT, 2, 64) == -1:
    raise RuntimeError("SDL2 Error: failed to open audio track")

mixer.Mix_AllocateChannels(1)
mixer.Mix_Volume(0, mixer.MIX_MAX_VOLUME)

for name in SFX_NAMES:
    path = Path(ASSET_PATH, name)
    sfx[name] = mixer.Mix_LoadWAV(str(path).encode("utf-8"))


class Hole:
    # 0: empty
    # 1: mole
    state = 0
    # in unix timestamp
    last_change = 0  # time of last state change
    due = 0  # if tick time is larger than this, pop up / retract

    def __init__(self, mode: int, win: curses.window):
        self.win = win
        self.mode = mode

        # TODO: levels
        self.alpha = random.uniform(4, 6)
        self.beta = random.uniform(3, 5)
        self.mul_pop = random.uniform(9, 15)
        self.mul_ret = random.uniform(1, 2)

    def init(self):
        self.update(0)

    def tick(self, status_bar: status.StatusBar):
        curr_time = time.time()

        if self.due < curr_time:
            if self.state == 0:
                if random.random() < 0.8:
                    self.update(1)
                else:
                    self.update(0)
            elif self.state == 1:
                if self.mode == 0:
                    status_bar.miss()
                else:
                    status_bar.update(-69.0)

                self.update(0)

    def update(self, state: int):
        curr_time = time.time()
        self.state = state
        self.last_change = curr_time
        self.win.clear()

        if state == 1:
            delay = random.betavariate(self.alpha, self.beta) * self.mul_ret
            delay = max(delay, 0.8)
            self.win.addstr(5, 0, assets["hole_popup.txt"])
        elif state == 0:
            delay = random.betavariate(self.alpha, self.beta) * self.mul_pop
            delay = max(delay, 1.0)
            self.win.addstr(7, 0, assets["hole_empty.txt"])

        self.due = curr_time + delay
        self.win.refresh()

    def hit(self, status_bar: status.StatusBar):
        curr_time = time.time()

        if self.state == 0:  # hitting an empty hole
            if self.mode == 0:
                status_bar.miss()
            else:
                score_change = -69.0
                status_bar.update(score_change)
        else:
            score_change = 6.9 / (curr_time - self.last_change)
            score_change = min(score_change, 69.0)
            self.last_change = curr_time
            delay = random.betavariate(self.alpha, self.beta) * self.mul_ret
            # self.due = curr_time + delay
            self.state = 0
            status_bar.update(score_change)

        self.win.immedok(True)
        self.win.addstr(0, 0, assets["hammer.txt"])
        mixer.Mix_PlayChannel(0, random.choice(list(sfx.values())), 0)
        curses.napms(32)
        self.win.addstr(0, 0, "\n\n\n\n\n")  # clear the previous hammer
        self.win.addstr(2, 0, assets["hammer.txt"])  # move down by 2
        curses.napms(48)
        self.win.clear()
        self.win.addstr(7, 0, assets["hole_empty.txt"])
        self.win.immedok(False)
